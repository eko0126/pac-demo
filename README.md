# pac-demo

### build jar
```
make build_jar
```

### run
```
make run
```

### test
```
curl http://127.0.0.1:8080/hello/
#Response: "Hello World!"
```
